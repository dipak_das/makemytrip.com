package Makemytrip.com;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Utility.AllWait;
import Utility.Testutil;

public class Makemytripmethods
{

	public static ChromeDriver driver;
	public static Properties prop;
	boolean found = false;
	
	public static void loadProperties() throws Exception
	{
		//("user.dir")+"\\Data\\login.properties")
	
	InputStream in=new FileInputStream(System.getProperty("user.dir")+"\\LoginDetails\\login.properties");
	prop = new  Properties();
	prop.load(in);
	
	}
	
	

	
	@BeforeClass
	 public static void BrowserLaunch() throws InterruptedException
		{
			
			
		 
			System.setProperty("webdriver.chrome.driver", ".//Driver//chromedriver.exe"); 
			
			// Create object of HashMap Class
				Map<String, Object> prefs = new HashMap<String, Object>();
						              
						                // Set the notification setting it will override the default setting
						prefs.put("profile.default_content_setting_values.notifications", 2);
						 
						                // Create object of ChromeOption class
						ChromeOptions options = new ChromeOptions();
						 
						                // Set the experimental option
						options.setExperimentalOption("prefs", prefs);
			
			
			
			driver = new ChromeDriver(options);
			driver.get("https://www.makemytrip.com/");
			driver.manage().window().maximize();
			
			
			
			
			
						
		}
	 
	 // Login
	 	 
	 @Test(priority=1)
	 public static void login() throws Exception
	 {
		 Makemytripmethods.loadProperties();
		 AllWait.waitExplicitly(driver, By.id("ch_login_icon"), "voe", 2000); 
		driver.findElement(By.xpath("//*[@id='ch_login_icon']")).click();
		Makemytripmethods.loadProperties();
		AllWait.waitExplicitly(driver, By.xpath("//input[@id='ch_login_email']"), "voe", 3000); 		
		//driver.findElement(By.xpath("//input[@id='ch_login_email']")).sendKeys("das.dipak19@gmail.com");
		driver.findElement(By.xpath("//input[@id='ch_login_email']")).sendKeys(prop.getProperty("email"));
		//System.out.println("email");
		//driver.findElement(By.xpath("//input[@id='ch_login_password']")).sendKeys("dipak123");
		driver.findElement(By.xpath("//input[@id='ch_login_password']")).sendKeys(prop.getProperty("password"));
		driver.findElement(By.xpath("//button[@id='ch_login_btn']")).click();
		Thread.sleep(5000);		
		
				
	}
	
	 
	 public static void MyAccount() throws InterruptedException 
	 {
		
		 
		 
			Actions action = new Actions(driver);
		 
	        action.moveToElement(driver.findElement(By.xpath("//div[@class='ch__userInteraction ch__clearfix']/ul/li[8]/span/a"))).perform();
		 
		 
		 
		 driver.findElement(By.xpath("//a[contains(text(),'My Account')]")).click();
		 
				 
		 driver.findElement(By.xpath("//a[contains(text(),' PROFILE')]")).click();
		 
		 AllWait.waitExplicitly(driver, By.xpath("//ul[@class='profile-list dynamic_mar_bot']/li/a"), "etc", 2000);
		 
		//driver.findElement(By.xpath("//ul[@class='profile-list dynamic_mar_bot']/li/a")).click();
		 driver.findElement(By.xpath("//a[contains(text(),'Travellers')]")).click();
		
		
	//AllWait.waitExplicitly(driver, By.xpath("//a[@class='ma-green-btn flR width70']"), "voe", 8000);
	
	//AllWait.waitExplicitly(driver, By.xpath("//a[contains(text(),'Add Name')]//parent::span//following-sibling::a[contains(text(),'Edit')]"), "voe", 8000);
	
	//driver.findElement(By.xpath("//a[@class='ma-green-btn flR width70']")).click();
	
	
	AllWait.waitExplicitly(driver, By.xpath("//div[@class='mboxDefault']"), "voe", 8000);
	
	driver.findElement(By.xpath("//*[@id='profile_cotr_pers_editbutton']")).click();
		
	 Select Title = new Select(driver.findElement(By.xpath("//*[@id='profile_editpersonal_title']")));
	 Title.selectByVisibleText("Mr.");
	 
	 
	WebElement Firstname= driver.findElement(By.xpath("//*[@id='profile_editpersonal_firstname']"));
	Firstname.sendKeys("Dipak");
	
	WebElement Middlename= driver.findElement(By.xpath("//*[@id='profile_editpersonal_middlename']"));
	Middlename.sendKeys("Kumar");
	
	WebElement Lastname= driver.findElement(By.xpath("//*[@id='profile_editpersonal_lastname']"));
	Lastname.sendKeys("Das");
	
	
	// Date Of Birth
	
	Select Day = new Select(driver.findElement(By.xpath("//*[@id='profile_editpersonal_dob_day']")));
	Day.selectByVisibleText("10");
	
	Select Month = new Select(driver.findElement(By.xpath("//*[@id='profile_editpersonal_dob_month']")));
	Month.selectByVisibleText("Mar");
	
	Select year = new Select(driver.findElement(By.xpath("//*[@id='profile_editpersonal_dob_year']")));
	year.selectByVisibleText("1980");
	
	//Your Complete Address
	
	WebElement Address= driver.findElement(By.xpath("//*[@id='profile_editpersonal_address']"));
	Address.sendKeys("45 B Jerry Miller");
	
	WebElement City= driver.findElement(By.xpath("//*[@id='profile_editpersonal_city']"));
	City.sendKeys("Kolkata");
	
	WebElement Pin= driver.findElement(By.xpath("//*[@id='profile_editpersonal_pincode']"));
	Pin.sendKeys("700081");
	
	Select Country = new Select(driver.findElement(By.xpath("//*[@id='profile_editpersonal_country']")));
	Country.selectByVisibleText("India");
	
	AllWait.waitExplicitly(driver, By.xpath("//*[@id='profile_editpersonal_state']"), "etc", 3000);
	
	Select State = new Select(driver.findElement(By.xpath("//*[@id='profile_editpersonal_state']")));
	State.selectByVisibleText("Goa");
	
	/*AllWait.waitExplicitly(driver, By.xpath("//a[@id='profile_editpersonal_removeimg']"), "etc", 8000);
	
	WebElement profileimage = driver.findElement(By.xpath("//a[@id='profile_editpersonal_removeimg']"));
	profileimage.click();*/
	
	//*[@id="profile_editpersonal_savebutton"]
	
	AllWait.waitExplicitly(driver, By.xpath("//*[@id='profile_editpersonal_savebutton']"), "etc", 8000);
	
	
	
	WebElement SaveProfile = driver.findElement(By.xpath("//*[@id='profile_editpersonal_savebutton']"));
	SaveProfile.click();
	
	
	
	//*[@id="profile_editpersonal_state"]
		
		 
		/* // driver.findElement(By.xpath("//*[@id='ch_logged-in']")).click();
		 Makemytripmethods.waitExplicitly(driver, By.id("ch_logged-inaccount"), "etc", 2000);
		 driver.findElement(By.xpath("//*[@id='ch_logged-inaccount']")).click();
		 driver.findElement(By.xpath("//a[@id='_ngcontent-ohl-263']")).click();
		 Makemytripmethods.waitExplicitly(driver, By.xpath("//ul[@class='profile-list dynamic_mar_bot']/li[1]"), "etc", 2000);
		 driver.findElement(By.xpath("//ul[@class='profile-list dynamic_mar_bot']/li[1]")).click();*/
		 
		
	 }
	 
	 @Test(dataProvider="gettestdata",priority=2)
	 public static void FlightSearch(String to,String day) throws InterruptedException
	 {
		 
		 driver.findElement(By.xpath("//*[@id='hp-widget__sfrom']")).click();
		 Thread.sleep(4000);
		 
		 Actions action = new Actions(driver);
		 
	    action.moveToElement(driver.findElement(By.xpath("//div[@class='locationFilter autocomplete_from']/ul/li[7]"))).click().build().perform();
		 
		 
		 
		 
		/* WebElement From= driver.findElement(By.xpath("//*[@id='hp-widget__sfrom']"));
		 From.clear();
		 From.sendKeys("Kolkata, India");*/
		 
		//div[@class='locationFilter autocomplete_from']/ul/li[7]
		 
		
		 WebElement To= driver.findElement(By.xpath("//*[@id='hp-widget__sTo']"));
		 To.clear();
		 To.sendKeys(to);
		// driver.findElement(By.id("hp-widget__depart")).click();
		 
		//*[@id="hp-widget__depart"]
		 
		 // For Departing Date 
		 
		 Thread.sleep(4000);
		 driver.findElement(By.xpath("//*[@id='hp-widget__depart']")).click();
		 
		 Thread.sleep(4000);
		 
		 List<WebElement> allDates=driver.findElements(By.xpath("//div[@class='dateFilter hasDatepicker']//table//tbody/tr//td//a"));
			
			for(WebElement ele:allDates)
			{
				
				String date=ele.getText();
				
				//System.out.println(date);
				if(date.equalsIgnoreCase(day))
				{
					ele.click();
					break;
				}
				
			}
	 
			// Return Date
			
			 Thread.sleep(5000);
			/* driver.findElement(By.xpath("//*[@id='hp-widget__return']")).click();
			 
			 Thread.sleep(4000);
			 
			 List<WebElement> allDates2=driver.findElements(By.xpath("//div[@class='dateFilterReturn hasDatepicker']//table//tbody/tr//td//a"));
				
				for(WebElement ele2:allDates2)
				{
					
					String date2=ele2.getText();
					
					//System.out.println(date2);
					
					if(date2.equalsIgnoreCase("31"))
					{
						ele2.click();
						break;
					}
					
				}*/
			
			
			// Passenger 
				
				//*[@id="hp-widget__paxCounter_pot"]
			
				driver.findElement(By.xpath("//*[@id='hp-widget__paxCounter_pot']")).click();
				
				driver.findElement(By.xpath("//div[@class='paxCounter']//li[2]")).click();
					
				driver.findElement(By.xpath("//*[@id='js-child_counter']/li[1]")).click();
				driver.findElement(By.xpath("//*[@id='js-infant_counter']/li[1]")).click();
				
			
				
				driver.findElement(By.xpath("//*[@id='economy']")).click();
				
				

				
				driver.findElement(By.xpath("//*[@id='pot_class']/a")).click();
			

				
				driver.findElement(By.xpath("//*[@id='searchBtn']")).click();
				
				
				// click lowast flight and
				Thread.sleep(3000);
				
				//http://qtpselenium.com/selenium-tutorial/forum/viewtopic.php?f=20&t=3196 (Copy Form This sction)
				
				/*WebElement flight1 = driver.findElement(By.xpath("//div[@class='col-lg-3 col-md-4 col-sm-4 col-xs-4 no-rt-padding']/span[1]"));
				flight1.click();*/
				
				//get all the prices from the table
				
				List <WebElement> price = driver.findElements(By.xpath("//*[@id='content']/div/div[6]/div[5]/div[2]/div[5]/div/div[2]/div[6]/p[1]/span[2]"));
				System.out.println("Your Price is : " + price.size());
				
				//put all the prices into array list and get the lowest prices
				ArrayList<Integer> prices=new ArrayList<Integer>();
				for(int i=0;i<price.size();i++){
				System.out.println(price.get(i).getText());
					Integer priceInt = Integer.valueOf(price.get(i).getText().replace(",", ""));
					prices.add(priceInt);
				}
				Integer minPrice = Collections.min(prices);
				System.out.println("Min Price is "+minPrice);
				
				//compare all the prices with lowest price and click the corresponding book button
						
				
				List<WebElement> allBookbtn = driver.findElements(By.xpath("//*[@id='content']/div/div[6]/div[5]/div[2]/div[5]/div/div[2]/div[7]/p/a"));
				Thread.sleep(3000);
				for(int i=0;i<price.size();i++){
				Integer priceInt1 = Integer.valueOf(price.get(i).getText().replace(",", ""));
				System.out.println(priceInt1);
				if (priceInt1.equals(minPrice)) 
				{
					allBookbtn.get(i).click();
					break;
					}
				
				
				}
				
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@id='content']/div[2]/div[5]/div[1]/div[1]/div[4]/div[4]/div[1]/div[2]/div/div[1]/label/span[1]")).click();
				Thread.sleep(4000);
				driver.findElement(By.xpath("//*[@id='continueToReview']")).click();
				
				
				
				
			/*   search highest fare flight*/
	/*			
	Thread.sleep(6000);
				
	WebElement dep_search_result_block=driver.findElement(By.xpath("//div[@class='col-xs-6 left_pannel']/div/div/p/span[2]/span[3]/span"));			
		
	String dep_search_result_count_text_full=dep_search_result_block.getText();	
	
	//System.out.println(dep_search_result_count_text_full);
	
	String dep_search_result_count_text_removed_text=dep_search_result_count_text_full.replaceAll("Flights found","");
	
	String dep_search_result_count_text=dep_search_result_count_text_removed_text.trim();
	
	//System.out.println(dep_search_result_count_text);
	
	
	
	/*
	WebElement arv_search_result_block=driver.findElement(By.xpath("//div[@class='col-xs-6 right_pannel']/div/div/p/span[2]/span[3]/span"));			
	
	String arv_search_result_count_text_full=arv_search_result_block.getText();
	
	//System.out.println(arv_search_result_count_text_full);	
	
    String arv_search_result_count_text_removed_text=arv_search_result_count_text_full.replaceAll("Flights found","");
	
	String arv_search_result_count_text=arv_search_result_count_text_removed_text.trim();
	
	//System.out.println(arv_search_result_count_text);
	*/
	
			
	 }
	 
	 @Test(priority=3)
	 public static void  Travellers() throws Exception
	 {
		 //Adult 1
		 Thread.sleep(5000);
		 
		 WebElement FirstName = driver.findElement(By.xpath("//span[contains(text(),'Adult 1')]//following-sibling::span/input[@placeholder='First Name']"));
		 FirstName.sendKeys("Jems");
		 
		 WebElement LastName = driver.findElement(By.xpath("//span[contains(text(),'Adult 1')]//following-sibling::span/input[@placeholder='Last Name']"));
		 LastName.sendKeys("Miller");
		 		
		 
		 WebElement Sex = driver.findElement(By.xpath("//span[contains(text(),'Adult 1')]//parent::p//following-sibling::p[contains(@class,'clearfix append_bottom')]//child::span/a[text()='FEMALE']"));
		 Sex.click();
		 
		 
		 //Adult 2
		 WebElement FirstName2 = driver.findElement(By.xpath("//span[contains(text(),'Adult 2')]//following-sibling::span/input[@placeholder='First Name']"));
		 FirstName2.sendKeys("Jerry");
		 
		 WebElement LastName2 = driver.findElement(By.xpath("//span[contains(text(),'Adult 2')]//following-sibling::span/input[@placeholder='Last Name']"));
		 LastName2.sendKeys("Miller");
		 
		 
		 WebElement Sex2= driver.findElement(By.xpath("//span[contains(text(),'Adult 2')]//parent::p//following-sibling::p[contains(@class,'clearfix append_bottom')]//child::span/a[text()='MALE']"));
		 Sex2.click();
		 
		 //Child 1
		 
		 		 
		 WebElement FirstName3 = driver.findElement(By.xpath("//span[contains(text(),'CHILD 1')]//following-sibling::span/input[@placeholder='First Name']"));
		 FirstName3.sendKeys("Mickey");
		 
		 WebElement LastName3 = driver.findElement(By.xpath("//span[contains(text(),'CHILD 1')]//following-sibling::span/input[@placeholder='Last Name']"));
		 LastName3.sendKeys("Miller");
		 
		 
		 
		 WebElement Sex3= driver.findElement(By.xpath("//span[contains(text(),'CHILD 1')]//parent::p//following-sibling::p[contains(@class,'clearfix append_bottom')]//child::span/a[text()='FEMALE']"));
		 Sex3.click();
		 
		 Thread.sleep(3000);
		 WebElement Age = driver.findElement(By.xpath("//*[@id='content']/div[2]/div[2]/div[4]/div[1]/div/div[1]/div/div/div[3]/div/p[3]/span[3]/input"));
		 Age.sendKeys("10");
		 
		 
		 
		 //Infant
		 WebElement FirstName4 = driver.findElement(By.xpath("//span[contains(text(),'Infant 1')]//following-sibling::span/input[@placeholder='First Name']"));
		 FirstName4.sendKeys("Mouse");
		 
		 WebElement LastName4 = driver.findElement(By.xpath("//span[contains(text(),'Infant 1')]//following-sibling::span/input[@placeholder='Last Name']"));
		 LastName4.sendKeys("Miller");
		 		 		 
		 WebElement Sex4 = driver.findElement(By.xpath("//span[contains(text(),'Infant 1')]//parent::p//following-sibling::p[contains(@class,'clearfix append_bottom')]//child::span/a[text()='MALE']"));
		 Sex4.click(); 
		 
		 Select drpAge = new Select(driver.findElement(By.xpath("//*[@id='content']/div[2]/div[2]/div[4]/div[1]/div/div[1]/div/div/div[4]/div/p[3]/span[4]/select")));
		 drpAge.selectByVisibleText("1 to 2 Year(s)"); 
		 
		 WebElement MobNumber = driver.findElement(By.xpath("//input[@placeholder='Mobile Number']"));
		 MobNumber.clear();
		 MobNumber.sendKeys("8584808211");
		 
		 Thread.sleep(3000);
		 WebElement PaymentButton = driver.findElement(By.xpath("//a[@class='btn btn-lg btn-primary-red col-lg-5 col-md-5 col-sm-5 col-xs-12 dblblack_bloker ng-binding']"));
		 PaymentButton.click();
		 
		 
		 
		 
	 }
	 
	 
	 
	 @DataProvider(name="gettestdata")
		public Iterator<Object[]> gettestdata(){
			
			ArrayList<Object[]> testdata=Testutil.getdatafromexcel();
			return testdata.iterator();
		}
	  
	 
	
	 	 
	 
	public static void closebrowser()
	{
		driver.close();
	}
	
	
	 
}
	
	
	
	

